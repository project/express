api = 2
core = 7.x

; MAKE file for Express used by Drupal.org packaging script

; Include the definition for how to build Drupal core directly, including patches:
includes[] = drupal-org-core.make

; Download the Express install profile and recursively build all its dependencies:
projects[express][type] = profile
projects[express][download][type] = git
projects[express][download][branch] = 7.x-2.x