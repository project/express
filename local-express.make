; express make file for local usage
core = "7.x"
api = "2"

;Drupal Core
projects[drupal][version] = "7.50"

; +++++ Modules +++++

projects[module_filter][version] = "2.0"
projects[module_filter][subdir] = "contrib"

projects[navbar][version] = "1.7"
projects[navbar][subdir] = "contrib"

projects[views_bulk_operations][version] = "3.3"
projects[views_bulk_operations][subdir] = "contrib"

projects[bean][version] = "1.11"
projects[bean][subdir] = "contrib"

projects[cck][version] = "3.0-alpha3"
projects[cck][subdir] = "contrib"

;projects[block_styles][version] = "1.0"
;projects[block_styles][subdir] = "contrib"

;projects[cu_accessibility][version] = "1.0"
;projects[cu_accessibility][subdir] = "contrib"

;projects[cu_alerts][version] = "1.0"
;projects[cu_alerts][subdir] = "contrib"

;projects[cu_back_to_top][version] = "1.0"
;projects[cu_back_to_top][subdir] = "contrib"

;projects[cu_bean_override_view][version] = "1.0"
;projects[cu_bean_override_view][subdir] = "contrib"

;projects[cu_block][version] = "1.0"
;projects[cu_block][subdir] = "contrib"

;projects[cu_content_administration_override][version] = "1.0"
;projects[cu_content_administration_override][subdir] = "contrib"

;projects[cu_content_lock][version] = "1.0"
;projects[cu_content_lock][subdir] = "contrib"

;projects[cu_core][version] = "2.7"
;projects[cu_core][subdir] = "contrib"

;projects[cu_date_formats][version] = "1.0"
;projects[cu_date_formats][subdir] = "contrib"

;projects[cu_faq][version] = "1.0"
;projects[cu_faq][subdir] = "contrib"

;projects[cu_font_awesome][version] = "1.0"
;projects[cu_font_awesome][subdir] = "contrib"

;projects[cu_give_buttons][version] = "1.0"
;projects[cu_give_buttons][subdir] = "contrib"

;projects[cu_help][version] = "1.0"
;projects[cu_help][subdir] = "contrib"

;projects[cu_hero_unit][version] = "1.0"
;projects[cu_hero_unit][subdir] = "contrib"

;projects[cu_home_page][version] = "1.0"
;projects[cu_home_page][subdir] = "contrib"

;projects[cu_image_styles][version] = "1.0"
;projects[cu_image_styles][subdir] = "contrib"

;projects[cu_inventory][version] = "1.0"
;projects[cu_inventory][subdir] = "contrib"

;projects[cu_inventory_stats][version] = "1.0"
;projects[cu_inventory_stats][subdir] = "contrib"

;projects[cu_jquery][version] = "1.0"
;projects[cu_jquery][subdir] = "contrib"

;projects[cu_ldap][version] = "1.0"
;projects[cu_ldap][subdir] = "contrib"

;projects[cu_page][version] = "1.0"
;projects[cu_page][subdir] = "contrib"

;projects[cu_people_administration_override][version] = "1.0"
;projects[cu_people_administration_override][subdir] = "contrib"

;projects[cu_redirect][version] = "1.0"
;projects[cu_redirect][subdir] = "contrib"

;projects[cu_search][version] = "1.1"
;projects[cu_search][subdir] = "contrib"

;projects[cu_shortcodes][version] = "1.0"
;projects[cu_shortcodes][subdir] = "contrib"

;projects[cu_shortcodes_wysiwyg][version] = "1.0"
;projects[cu_shortcodes_wysiwyg][subdir] = "contrib"

;projects[cu_site_info][version] = "1.0"
;projects[cu_site_info][subdir] = "contrib"

;projects[cu_sitewide][version] = "1.0"
;projects[cu_sitewide][subdir] = "contrib"

;projects[cu_slider][version] = "1.0"
;projects[cu_slider][subdir] = "contrib"

;projects[cu_users][version] = "1.0"
;projects[cu_users][subdir] = "contrib"

;projects[cu_view_modes][version] = "1.0"
;projects[cu_view_modes][subdir] = "contrib"

;projects[cu_wysiwyg][version] = "1.0"
;projects[cu_wysiwyg][subdir] = "contrib"

;projects[express_dashboards][version] = "1.0"
;projects[express_dashboards][subdir] = "contrib"

;projects[files][version] = "1.0"
;projects[files][subdir] = "contrib"

;projects[grid_size_blocks][version] = "1.0"
;projects[grid_size_blocks][subdir] = "contrib"

;projects[site_navigation_menus][version] = "1.0"
;projects[site_navigation_menus][subdir] = "contrib"

projects[varnish][version] = "1.1"
projects[varnish][subdir] = "contrib"

projects[ctools][version] = "1.10"
projects[ctools][subdir] = "contrib"

projects[context][version] = "3.7"
projects[context][subdir] = "contrib"

projects[context_accordion][version] = "1.0"
projects[context_accordion][subdir] = "contrib"

projects[date][version] = "2.9"
projects[date][subdir] = "contrib"

projects[ds][version] = "2.14"
projects[ds][subdir] = "contrib"

;projects[express_add_content][version] = "1.0"
;projects[express_add_content][subdir] = "contrib"

projects[express_help][version] = "1.x-dev"
projects[express_help][subdir] = "contrib"

projects[express_theme_picker][version] = "1.x-dev"
projects[express_theme_picker][subdir] = "contrib"

projects[features][version] = "2.10"
projects[features][subdir] = "contrib"

projects[email][version] = "1.3"
projects[email][subdir] = "contrib"

projects[entityreference][version] = "1.2"
projects[entityreference][subdir] = "contrib"

projects[fences][version] = "1.2"
projects[fences][subdir] = "contrib"

projects[field_collection][version] = "1.0-beta11"
projects[field_collection][subdir] = "contrib"

projects[field_group][version] = "1.5"
projects[field_group][subdir] = "contrib"

projects[files_undo_remove][version] = "1.3"
projects[files_undo_remove][subdir] = "contrib"

projects[inline_entity_form][version] = "1.8"
projects[inline_entity_form][subdir] = "contrib"

projects[link][version] = "1.4"
projects[link][subdir] = "contrib"

projects[smart_trim][version] = "1.5"
projects[smart_trim][subdir] = "contrib"

projects[taxonomy_formatter][version] = "1.4"
projects[taxonomy_formatter][subdir] = "contrib"

projects[flexslider][version] = "2.0-rc1"
projects[flexslider][subdir] = "contrib"

projects[flexslider_views_slideshow][version] = "2.x-dev"
projects[flexslider_views_slideshow][subdir] = "contrib"

projects[modernizr][version] = "3.9"
projects[modernizr][subdir] = "contrib"

projects[google_appliance][version] = "1.14"
projects[google_appliance][subdir] = "contrib"

projects[smartcrop][version] = "1.x-dev"
projects[smartcrop][subdir] = "contrib"

projects[pathologic][version] = "2.12"
projects[pathologic][subdir] = "contrib"

projects[video_filter][version] = "3.4"
projects[video_filter][subdir] = "contrib"

projects[wysiwyg_filter][version] = "1.6-rc3"
projects[wysiwyg_filter][subdir] = "contrib"

projects[ldap][version] = "2.0-beta11"
projects[ldap][subdir] = "contrib"

projects[file_entity][version] = "2.0-beta3"
projects[file_entity][subdir] = "contrib"

projects[video_embed_field][version] = "2.0-beta11"
projects[video_embed_field][subdir] = "contrib"

projects[administerusersbyrole][version] = "2.0"
projects[administerusersbyrole][subdir] = "contrib"

projects[attachment_links][version] = "1.x-dev"
projects[attachment_links][subdir] = "contrib"

projects[auto_entitylabel][version] = "1.3"
projects[auto_entitylabel][subdir] = "contrib"

projects[bigmenu][version] = "1.3"
projects[bigmenu][subdir] = "contrib"

projects[blocktheme][version] = "1.1"
projects[blocktheme][subdir] = "contrib"

projects[chain_menu_access][version] = "2.0"
projects[chain_menu_access][subdir] = "contrib"

projects[colorbox][version] = "2.12"
projects[colorbox][subdir] = "contrib"

projects[content_lock][version] = "2.1"
projects[content_lock][subdir] = "contrib"

projects[disable_messages][version] = "1.1"
projects[disable_messages][subdir] = "contrib"

projects[disable_node_menu_item][version] = "1.1"
projects[disable_node_menu_item][subdir] = "contrib"

projects[entity][version] = "1.8"
projects[entity][subdir] = "contrib"

projects[fitvids][version] = "1.17"
projects[fitvids][subdir] = "contrib"

projects[focal_point][version] = "1.0"
projects[focal_point][subdir] = "contrib"

projects[image_url_formatter][version] = "1.4"
projects[image_url_formatter][subdir] = "contrib"

projects[insert][version] = "1.3"
projects[insert][subdir] = "contrib"

projects[libraries][version] = "2.3"
projects[libraries][subdir] = "contrib"

projects[linkit][version] = "3.5"
projects[linkit][subdir] = "contrib"

projects[menu_block][version] = "2.7"
projects[menu_block][subdir] = "contrib"

projects[menu_firstchild][version] = "1.1"
projects[menu_firstchild][subdir] = "contrib"

projects[navigation404][version] = "1.0"
projects[navigation404][subdir] = "contrib"

projects[pathauto][version] = "1.3"
projects[pathauto][subdir] = "contrib"

projects[redirect][version] = "1.0-rc3"
projects[redirect][subdir] = "contrib"

projects[role_delegation][version] = "1.1"
projects[role_delegation][subdir] = "contrib"

projects[secure_permissions][version] = "2.x-dev"
projects[secure_permissions][subdir] = "contrib"

projects[securepages][version] = "1.0-beta2"
projects[securepages][subdir] = "contrib"

projects[stringoverrides][version] = "1.8"
projects[stringoverrides][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[token][version] = "1.6"
projects[token][subdir] = "contrib"

projects[transliteration][version] = "3.2"
projects[transliteration][subdir] = "contrib"

projects[vppr][version] = "1.0"
projects[vppr][subdir] = "contrib"

projects[globalredirect][version] = "1.5"
projects[globalredirect][subdir] = "contrib"

projects[noreqnewpass][version] = "1.2"
projects[noreqnewpass][subdir] = "contrib"

projects[entitycache][version] = "1.5"
projects[entitycache][subdir] = "contrib"

projects[memcache][version] = "1.5"
projects[memcache][subdir] = "contrib"

projects[view_unpublished][version] = "1.2"
projects[view_unpublished][subdir] = "contrib"

projects[shortcode][version] = "2.22"
projects[shortcode][subdir] = "contrib"

projects[honeypot][version] = "1.22"
projects[honeypot][subdir] = "contrib"

projects[google_analytics][version] = "2.3"
projects[google_analytics][subdir] = "contrib"

projects[profile_module_manager][version] = "1.0"
projects[profile_module_manager][subdir] = "contrib"

projects[ckeditor_link][version] = "2.4"
projects[ckeditor_link][subdir] = "contrib"

projects[content_menu][version] = "1.x-dev"
projects[content_menu][subdir] = "contrib"

projects[jquery_update][version] = "3.0-alpha3"
projects[jquery_update][subdir] = "contrib"

projects[responsive_preview][version] = "1.x-dev"
projects[responsive_preview][subdir] = "contrib"

projects[wysiwyg][version] = "2.x-dev"
projects[wysiwyg][subdir] = "contrib"

projects[eva][version] = "1.3"
projects[eva][subdir] = "contrib"

projects[user_external_invite][version] = "1.0"
projects[user_external_invite][subdir] = "contrib"

projects[views][version] = "3.14"
projects[views][subdir] = "contrib"

projects[views_accordion][version] = "1.1"
projects[views_accordion][subdir] = "contrib"

projects[views_bulk_operations][version] = "3.3"
projects[views_bulk_operations][subdir] = "contrib"

projects[views_content_cache][version] = "3.0-alpha3"
projects[views_content_cache][subdir] = "contrib"

projects[views_slideshow][version] = "3.1"
projects[views_slideshow][subdir] = "contrib"

; +++++ TODO modules without versions +++++

;projects[express_settings][version] = "" ; TODO add version
;projects[express_settings][subdir] = "contrib"

; +++++ Themes +++++

; omega
projects[omega][type] = "theme"
projects[omega][version] = "3.1"
projects[omega][subdir] = "contrib"

; +++++ Libraries +++++

; CKEditor
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"
libraries[ckeditor][destination] = "libraries"
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.6.1/ckeditor_3.6.6.1.tar.gz"

; ColorBox
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][type] = "library"
libraries[colorbox][destination] = "libraries"
libraries[colorbox][download][type] = "get"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/master.zip"

; FitVids
libraries[fitvids][directory_name] = "fitvids"
libraries[fitvids][type] = "library"
libraries[fitvids][destination] = "libraries"
libraries[fitvids][download][type] = "get"
libraries[fitvids][download][url] = "https://raw.github.com/davatron5000/FitVids.js/master/jquery.fitvids.js"

; Flexslider
libraries[flexslider][directory_name] = "flexslider"
libraries[flexslider][type] = "library"
libraries[flexslider][destination] = "libraries"
libraries[flexslider][download][type] = "get"
libraries[flexslider][download][url] = "https://github.com/woothemes/FlexSlider/archive/flexslider1.zip"

; +++++ Patches +++++

projects[field_collection][patch][] = "http://drupal.org/files/issue_1329856_1.patch"

